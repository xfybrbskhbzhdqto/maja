// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 5-1-0 : DM : LAIG-DM-MAC-1769-CNES : 6 juillet 2016 : Implémentation écart type checktools     *
 * VERSION : 5-1-0 : FA : LAIG-FA-MAC-145739-CS : 27 juin 2016 : Audit code - Supp de la macro ITK_EXPORT   *
 * VERSION : 1-0-0-3 : DM : 251 : 17 aout 2011 : Modif. pour prise en compte DM 251 Checktools dans SMIGS     *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 15 sept. 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsL1Extract_h
#define __vnsL1Extract_h

#include "itkProcessObject.h"
#include "vnsExtractBase.h"
#include "otbMultiChannelExtractROI.h"
#include "otbStreamingStatisticsImageFilter.h"
#include "vnsStreamingMaskConditionalStatisticsVectorImageFilter.h"
#include "otbExtractROI.h"
#include "itkAddImageFilter.h"

#include "vnsMacro.h"

namespace vns
{
    /** \class  L1Extract
     * \brief Extract TOAmeans, CloudVal and MissingDataVal from L1 product.
     * input Points list where information are extracted are given using
     * the SetListPixelLineId and SetListPixelColId methods
     * the radius is set by the SetRadius method. The default radius value is 0
     * which means that the information extracted is the pixel value.
     * Otherwise extracted information is the pixel neighborhood mean.
     *
     * \author CS Systemes d'Information
     *
     * \sa ProcessObject
     *
     * \ingroup L2
     * \ingroup L3
     * \ingroup Checktool
     *
     */

    template<class TInputImage, class TPIXInputMask, class TCLDInputMask>
        class L1Extract : public ExtractBase
        {
        public:
            /** Standard class typedefs. */
            typedef L1Extract Self;
            typedef ExtractBase Superclass;
            typedef itk::SmartPointer<Self> Pointer;
            typedef itk::SmartPointer<const Self> ConstPointer;

            /** Some convenient typedefs. */
            typedef TInputImage InputImageType;
            typedef typename InputImageType::Pointer InputImagePointer;
            typedef typename InputImageType::ConstPointer InputImageConstPointer;
            typedef typename InputImageType::PixelType PixelType;
            typedef typename InputImageType::InternalPixelType InternalPixelType;
            typedef typename InputImageType::IndexType IndexType;
            typedef typename InputImageType::SizeType SizeType;
            typedef typename InputImageType::RegionType RegionType;

            typedef TPIXInputMask PIXInputMaskType;
            typedef typename PIXInputMaskType::PixelType PIXPixelType;
            typedef typename PIXInputMaskType::Pointer PIXInputMaskPointer;

            typedef TCLDInputMask CLDInputMaskType;
            typedef typename CLDInputMaskType::PixelType CLDPixelType;
            typedef typename CLDInputMaskType::Pointer CLDInputMaskPointer;

            typedef typename Superclass::IndexVectorType IndexVectorType;
            typedef unsigned char UCType;

            typedef otb::ExtractROI<CLDPixelType, PIXPixelType> CLDExtractROIType;
            typedef otb::ExtractROI<PIXPixelType, PIXPixelType> PIXExtractROIType;

            typedef otb::MultiChannelExtractROI<InternalPixelType, InternalPixelType> MultiChannelExtractROIType;
            typedef typename MultiChannelExtractROIType::Pointer MultiChannelExtractROIPointer;

            typedef itk::AddImageFilter<PIXInputMaskType, PIXInputMaskType, PIXInputMaskType> AdderType;

            typedef otb::StreamingStatisticsImageFilter<PIXInputMaskType> StatisticsMaskFilterType;
            typedef typename StatisticsMaskFilterType::Pointer StatisticsMaskFilterPointer;

            typedef StreamingMaskConditionalStatisticsVectorImageFilter<InputImageType, PIXInputMaskType> StatisticsImageFilterType;
            typedef typename StatisticsImageFilterType::Pointer StatisticsImageFilterPointer;

            /** Type macro */
            itkNewMacro(Self)

            /** Creation through object factory macro */
            itkTypeMacro(L1Extract, ExtractBase )


            itkSetObjectMacro(TOAImage, InputImageType)

            itkSetObjectMacro(PIXMask, PIXInputMaskType)

            itkSetObjectMacro(CLDMask, CLDInputMaskType)

            itkSetMacro(NoData, RealNoDataType)


            //Get output vector
            vnsGetConstReferenceMacro(TOAMeans, PixelType )

			vnsGetConstReferenceMacro(TOAStDv, PixelType )

			vnsGetConstReferenceMacro(UsefulPixelPerc, PixelType )

			vnsGetConstReferenceMacro(ValidsPixelPerc, PixelType )

            vnsGetConstReferenceMacro(CloudVal, UCType )

            vnsGetConstReferenceMacro(MissingDataVal, UCType )

            /** Get if the TOA statistics of the band is valid */
            itkGetConstMacro(TOAStatIsValid,bool)


        protected:
            /** Constructor */
            L1Extract();
            /** Destructor */
            virtual
            ~L1Extract();
            /** PrintSelf method */
            virtual void
            PrintSelf(std::ostream& os, itk::Indent indent) const;

            virtual void
            ComputeIndexStat(IndexType & id);

        private:
            L1Extract(const Self&); //purposely not implemented
            void
            operator=(const Self&); //purposely not implemented

            //Input
            InputImagePointer m_TOAImage;
            CLDInputMaskPointer m_CLDMask;
            PIXInputMaskPointer m_PIXMask;
            RealNoDataType m_NoData;
            //Outputs
            PixelType m_TOAMeans;
            PixelType m_TOAStDv;
            PixelType m_UsefulPixelPerc;
            PixelType m_ValidsPixelPerc;
            UCType m_CloudVal;
            UCType m_MissingDataVal;

            /** Check if the TOA statistics of the band is valid */
            bool m_TOAStatIsValid;

        };

} // End namespace vns
#ifndef VNS_MANUAL_INSTANTIATION
#include "vnsL1Extract.txx"
#endif

#endif /* __vnsL1Extract_h */
