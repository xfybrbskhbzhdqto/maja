// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 2.1.0 : DM : LAIG-DM-MAJA-157814-CS : 18 mai 2017 : Refactoring, menage pour ameliorer qualite *
 * VERSION : 2.0.0 : DM : LAIG-DM-MAJA-153176-CS : 3 fevrier 2017 : Modifications pour désactiver           *
 *                          l'incrustation de texte dans les images jpg des produits QCK. Suppression de la *
 *                          gestion de masque dasn les baseline pour que les comparaisons des résultats sur *
 *                          les redhat 5, 6 et 7 soient plus simples à géréer                               *
 * VERSION : 1.0.0 : FA : LAIG-FA-MAC-148399-CS : 07 novembre 2016 : Audit code - Correction qualite        *
 * VERSION : 5-1-0 : FA : LAIG-FA-MAC-145739-CS : 27 juin 2016 : Audit code - Supp de la macro ITK_EXPORT   *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 31 août 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsL2QLCompositeVectorImageFilter_h
#define __vnsL2QLCompositeVectorImageFilter_h

#include <vnsChangeValueWithMaskVectorFunctor.h>
#include "vnsChangeValueVectorFunctor.h"
#include "itkImageToImageFilter.h"
#include "otbMultiChannelExtractROI.h"
#include "vnsLinearSubSampleVectorImageFilter.h"
#include "vnsLinearSubSampleImageFilter.h"
#include "otbVectorRescaleIntensityImageFilter.h"
#include "vnsL2QLExtractEdge.h"
#include "vnsWriteTxtOnImageFilter.h"
#include "otbImageFileReader.h"
#include "vnsMacro.h"

namespace vns
{
    /** \class  L2QLCompositeVectorImageFilter
     * \brief This composite filter take an input otb::VectorImage and give an output otb::VectorImage of
     * 3 channels. The output image channel are selected in the input image using SetQLRedBand, SetQLGreenBand and SetQLBlueBand.
     * Moreover the shadow, cloud and water mask (given as inputs) are printed in the output image
     * with the correponding value (shadowLabel, cloudLabel, WaterLabel).
     * The output image is also resample by a factor given by SetRatio.
     *
     *
     * \author CS Systemes d'Information
     *
     * \sa ImageToImageFilter
     *
     * \ingroup L2
     * \ingroup L3
     * \ingroup Checktool
     *
     */
    template<class TInputImage, class TMaskImage, class TOutputImage>
        class L2QLCompositeVectorImageFilter : public itk::ImageToImageFilter<TInputImage, TOutputImage>
        {
        public:
            /** Standard class typedefs. */
            typedef L2QLCompositeVectorImageFilter Self;
            typedef itk::ImageToImageFilter<TInputImage, TOutputImage> Superclass;
            typedef itk::SmartPointer<Self> Pointer;
            typedef itk::SmartPointer<const Self> ConstPointer;

            /** Type macro */
            itkNewMacro(Self)

            /** Creation through object factory macro */
            itkTypeMacro(L2QLCompositeVectorImageFilter, ImageToImageFilter )

            /** Some convenient typedefs. */
            typedef typename Superclass::InputImageType InputImageType;
            typedef typename InputImageType::Pointer InputImagePointer;
            typedef typename InputImageType::ConstPointer InputImageConstPointer;
            typedef typename InputImageType::RegionType RegionType;
            typedef typename InputImageType::IndexType IndexType;
            typedef typename InputImageType::PixelType InputImagePixelType;
            typedef typename InputImageType::InternalPixelType InputImageInternalPixelType;
            typedef typename InputImageType::SizeType SizeType;
            typedef typename Superclass::OutputImageType OutputImageType;
            typedef typename OutputImageType::Pointer OutputImagePointer;
            typedef typename OutputImageType::PixelType OutputImagePixelType;
            typedef typename OutputImageType::InternalPixelType OutputImageInternalPixelType;
            typedef TMaskImage MaskImageType;
            typedef typename MaskImageType::InternalPixelType MaskImageInternalPixelType;
            typedef typename MaskImageType::PixelType MaskImagePixelType;
            typedef typename MaskImageType::ConstPointer MaskImageConstPointer;
            typedef typename MaskImageType::Pointer MaskImagePointer;

            typedef LinearSubSampleVectorImageFilter<InputImageType, InputImageType> QuicklookImageGeneratorType;
            typedef LinearSubSampleImageFilter<MaskImageType, MaskImageType> QuicklookMaskGeneratorType;

            typedef otb::MultiChannelExtractROI<InputImageInternalPixelType, InputImageInternalPixelType> MultiChannelExtractROIFilterType;
            typedef Functor::ChangeValueWithMaskVectorFunctor<OutputImagePixelType, MaskImagePixelType, OutputImagePixelType>
                    ChangeValueVectorFunctorType;
            typedef itk::BinaryFunctorImageFilter<OutputImageType, MaskImageType, OutputImageType,
                    ChangeValueVectorFunctorType> PrintableImageFilterType;
            typedef otb::VectorRescaleIntensityImageFilter<InputImageType, OutputImageType> VectorRescaleIntensityImageFilterType;
            typedef L2QLExtractEdge<MaskImageType, MaskImageType> L2QLExtractEdgeFilterType;
            typedef WriteTxtOnImageFilter<OutputImageType, OutputImageType> WriteTxtOnImageFilterType;

            typedef typename MultiChannelExtractROIFilterType::Pointer MultiChannelExtractROIFilterPointerType;
            typedef Functor::VectorChangeValue<InputImagePixelType, InputImagePixelType> VectorChangeFunctorType;
            typedef itk::UnaryFunctorImageFilter<InputImageType, InputImageType, VectorChangeFunctorType> ChangeValueVectorImageFilterType;
            typedef typename ChangeValueVectorImageFilterType::Pointer ChangeValueVectorImageFilterPointerType;
            typedef typename PrintableImageFilterType::Pointer PrintableImageFilterPointerType;
            typedef typename QuicklookImageGeneratorType::Pointer QuicklookImageGeneratorPointerType;
            typedef typename QuicklookMaskGeneratorType::Pointer QuicklookMaskGeneratorPointerType;
            typedef typename VectorRescaleIntensityImageFilterType::Pointer VectorRescaleIntensityImageFilterPointerType;
            typedef typename L2QLExtractEdgeFilterType::Pointer L2QLExtractEdgeFilterPointerType;
            typedef typename WriteTxtOnImageFilterType::Pointer WriteTxtOnImageFilterPointerType;

            typedef otb::ImageFileReader<OutputImageType> PrintSHDReaderType;
            typedef typename PrintSHDReaderType::Pointer PrintSHDReaderPointerType;

            vnsSetGetInputRawMacro( InputImage, InputImageType, 0)
            vnsSetGetInputRawMacro( InputWATMask, MaskImageType, 1)
            vnsSetGetInputRawMacro( InputCLDMask, MaskImageType, 2)
            vnsSetGetInputRawMacro( InputSHDMask, MaskImageType, 3)
            vnsSetGetInputRawMacro( InputSNWMask, MaskImageType, 4)


            vnsMemberAndSetAndGetConstReferenceMacro(BackgroundMaskValue , MaskImageInternalPixelType)

            vnsUIntMemberAndGetConstReferenceMacro(Ratio)
            vnsUIntMemberAndGetConstReferenceMacro(QLRedBand)
            vnsUIntMemberAndGetConstReferenceMacro(QLGreenBand)
            vnsUIntMemberAndGetConstReferenceMacro(QLBlueBand)

            vnsMemberAndSetAndGetConstReferenceMacro( MinValueBRed, InputImageInternalPixelType)
            vnsMemberAndSetAndGetConstReferenceMacro( MinValueBGreen, InputImageInternalPixelType)
            vnsMemberAndSetAndGetConstReferenceMacro( MinValueBBlue, InputImageInternalPixelType)
            vnsMemberAndSetAndGetConstReferenceMacro( MaxValueBRed, InputImageInternalPixelType)
            vnsMemberAndSetAndGetConstReferenceMacro( MaxValueBGreen, InputImageInternalPixelType)
            vnsMemberAndSetAndGetConstReferenceMacro( MaxValueBBlue, InputImageInternalPixelType)
            vnsMemberAndSetAndGetConstReferenceMacro( NoData, InputImageInternalPixelType)
            vnsMemberAndSetAndGetConstReferenceMacro( NoDataReplaceValue, OutputImageInternalPixelType)

            vnsMemberAndSetAndGetConstReferenceMacro( CloudLabel, OutputImagePixelType)
            vnsMemberAndSetAndGetConstReferenceMacro( ShadowLabel, OutputImagePixelType)
            vnsMemberAndSetAndGetConstReferenceMacro( WaterLabel, OutputImagePixelType)
            vnsMemberAndSetAndGetConstReferenceMacro( SnowLabel, OutputImagePixelType)
            vnsMemberAndSetAndGetConstReferenceMacro( OutputMin, OutputImageInternalPixelType)
            vnsMemberAndSetAndGetConstReferenceMacro( OutputMax, OutputImageInternalPixelType)

            /** Set the Font file name */
            otbSetObjectMemberMacro(WriteTxtOnImageFilter,FontFileName, std::string)

            /** Set the Text */
            otbSetObjectMemberMacro(WriteTxtOnImageFilter,Text, std::string)

            /** Font size for GD library */
            otbSetObjectMemberMacro(WriteTxtOnImageFilter,FontSize, unsigned int)

            /** Set the Text Color */
            void
            SetTextColor(int redVal, int greenVal, int blueVal)
            {
                m_WriteTxtOnImageFilter->SetTextColor(redVal, greenVal, blueVal);
            }

            virtual OutputImageType *
            GetOutput()
            {
                return m_OutputImage;
            }

            /** Composite Filter method. */
            void
            UpdateData(void);

        protected:
            /** Constructor */
            L2QLCompositeVectorImageFilter();
            /** Destructor */
            virtual
            ~L2QLCompositeVectorImageFilter();
            /** Composite Filter method. */

            /** PrintSelf method */
            virtual void
            PrintSelf(std::ostream& os, itk::Indent indent) const;

            virtual void
            GenerateOutputInformation(void);

        private:
            L2QLCompositeVectorImageFilter(const Self&); //purposely not implemented
            void
            operator=(const Self&); //purposely not implemented

            MultiChannelExtractROIFilterPointerType m_MultiChannelExtractROIFilter;
            ChangeValueVectorImageFilterPointerType m_ChangeValueVectorImageFilter;
            PrintableImageFilterPointerType m_PrintCLDFilter;
            PrintableImageFilterPointerType m_PrintWATFilter;
            PrintableImageFilterPointerType m_PrintSHDFilter;
            PrintableImageFilterPointerType m_PrintSNWFilter;
            QuicklookImageGeneratorPointerType m_QuicklookGenerator;
            VectorRescaleIntensityImageFilterPointerType m_VectorRescaleIntensityImageFilter;

            QuicklookMaskGeneratorPointerType m_CloudQuicklookGenerator;
            QuicklookMaskGeneratorPointerType m_WaterQuicklookGenerator;
            QuicklookMaskGeneratorPointerType m_ShadowQuicklookGenerator;
            QuicklookMaskGeneratorPointerType m_SnowQuicklookGenerator;

            L2QLExtractEdgeFilterPointerType m_CLDExtractEdgeFilter;
            L2QLExtractEdgeFilterPointerType m_WATExtractEdgeFilter;
            L2QLExtractEdgeFilterPointerType m_SHDExtractEdgeFilter;
            L2QLExtractEdgeFilterPointerType m_SNWExtractEdgeFilter;
            WriteTxtOnImageFilterPointerType m_WriteTxtOnImageFilter;

            PrintSHDReaderPointerType m_PrintMaskReader;
            OutputImagePointer m_OutputImage;
        };

} // End namespace vns
#ifndef VNS_MANUAL_INSTANTIATION
#include "vnsL2QLCompositeVectorImageFilter.txx"
#endif

#endif /* __vnsL2QLCompositeVectorImageFilter_h */
