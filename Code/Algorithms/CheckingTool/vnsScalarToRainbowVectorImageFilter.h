// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 1.0.0 : FA : LAIG-FA-MAC-148399-CS : 03 novembre 2016 : Audit code - Correction qualite        *
 * VERSION : 5-1-0 : FA : LAIG-FA-MAC-145739-CS : 27 juin 2016 : Audit code - Supp de la macro ITK_EXPORT   *
 * VERSION : 5-0-0 : FA : LAIG-FA-MAC-144620-CS : 26 avril 2016 : Correction erreur type variable m_Hinc    *
 * VERSION : 4-7-3 : FA : LAIG-FA-MAC-141974-CS : 15 fevrier 2016 : Controle sur les bornes Min et Max pour *
 *                                                                 l' adaptation de la dynamique de l'image *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 24 sept. 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsScalarToRainbowVectorImageFilter_h
#define __vnsScalarToRainbowVectorImageFilter_h

#include "itkImageToImageFilter.h"
#include "itkUnaryFunctorImageFilter.h"
#include "itkConceptChecking.h"
#include "itkSimpleDataObjectDecorator.h"
#include "vnsMacro.h"

namespace vns
{

    namespace Functor
    {

        /** \class HSVToVariableLengthVectorFunctor
         * \brief Function object to convert HSV value to RGB.
         *
         *
         * \sa ScalarToRainbowRGBPixelFunctor
         *
         */
        template<class TPixel>
            class HSVToVariableLengthVectorFunctor
            {
            public:
                HSVToVariableLengthVectorFunctor()
                {
                }

                ~HSVToVariableLengthVectorFunctor()
                {
                }

                inline TPixel
                operator()(double h, double s, double v) const
                {
                    //Init the onethird value
                    const double onethird = 1.0 / 3.0;
                    //Init the onesixth value
                    const double onesixth = 1.0 / 6.0;
                    //Init the twothird value
                    const double twothird = 2.0 / 3.0;
                    //Init the fivesixth value
                    const double fivesixth = 5.0 / 6.0;
                    //Init rgb values
                    double r(0.);
                    double g(0.);
                    double b(0.);

                    // compute RGB from HSV
                    // green/red
                    if ((h > onesixth) && (h <= onethird))
                    {
                        //Set the r, g and b values
                        g = 1.0;
                        r = (onethird - h) / onesixth;
                        b = 0.0;
                    }
                    else
                    {
                        // green/blue
                        if ((h > onethird) && (h <= 0.5))
                        {
                            //Set the r, g and b values
                            g = 1.0;
                            b = (h - onethird) / onesixth;
                            r = 0.0;
                        }
                        else
                        {
                            // blue/green
                            if ((h > 0.5) && (h <= twothird))
                            {
                                //Set the r, g and b values
                                b = 1.0;
                                g = (twothird - h) / onesixth;
                                r = 0.0;
                            }
                            else
                            {
                                // blue/red
                                if ((h > twothird) && (h <= fivesixth))
                                {
                                    //Set the r, g and b values
                                    b = 1.0;
                                    r = (h - twothird) / onesixth;
                                    g = 0.0;
                                }
                                else
                                {
                                    // red/blue
                                    if ((h > fivesixth) && (h <= 1.0))
                                    {
                                        //Set the r, g and b values
                                        r = 1.0;
                                        b = (1.0 - h) / onesixth;
                                        g = 0.0;
                                    }
                                    // red/green
                                    else
                                    {
                                        //Set the r, g and b values
                                        r = 1.0;
                                        g = h / onesixth;
                                        b = 0.0;
                                    }
                                }
                            }

                        }
                    }
                    // add Saturation to the equation.
                    r = (s * r + (1.0 - s));
                    g = (s * g + (1.0 - s));
                    b = (s * b + (1.0 - s));

                    // apply v coef
                    r = r * v;
                    g = g * v;
                    b = b * v;

                    // Init ans value
                    TPixel ans;
                    ans.SetSize(3);
                    typedef typename TPixel::ValueType ValueType;
                    // Set the value per band
                    ans[0] = static_cast<ValueType>(r);
                    ans[1] = static_cast<ValueType>(g);
                    ans[2] = static_cast<ValueType>(b);
                    return ans;
                }
            };
    }

    namespace Functor
    {

        /** \class ScalarToRainbowVariableLengthVectorPixelFunctor
         * \brief Function object which maps a scalar value into a rainbow variableLengthVector pixel value.
         *
         *  Grayscale image is mapped to a color image where blue represents
         * small values and red represents big values.
         *
         * \example BasicFilters/DEMToRainbowExample.cxx
         *
         */
        template<class TScalar, class TPixel>
            class ScalarToRainbowVariableLengthVectorPixelFunctor /*:
             public itk::Functor::ScalarToRGBPixelFunctor<TScalar>*/
            {
            public:
                ScalarToRainbowVariableLengthVectorPixelFunctor();
                ~ScalarToRainbowVariableLengthVectorPixelFunctor()
                {
                }

                typedef TScalar ScalarType;
                typedef HSVToVariableLengthVectorFunctor<TPixel> HSVToVariableLengthVectorFunctorType;
                typedef typename TPixel::ValueType ValueType;

                inline TPixel
                operator()(const TScalar &) const;

                /** Set the input maximum to be mapped to red */
                void
                SetMinimumMaximum(const ScalarType & min, const ScalarType & max)
                {
                    this->m_Minimum = min;
                    this->m_Maximum = max;
                    m_Hinc = 0.;
                    if (this->m_Minimum < this->m_Maximum)
                    {
                        m_Hinc = 0.6 / (this->m_Maximum - this->m_Minimum);
                    }
                }

            protected:
                TPixel
                HSVToRGB(double h, double s, double v) const;

            private:
                double m_Hinc;
                ScalarType m_Maximum;
                ScalarType m_Minimum;
                HSVToVariableLengthVectorFunctorType m_HSVToVariableLengthVectorFunctor;
            };

    } // end namespace functor

    /** \class ScalarToRainbowVectorImageFilter
     * \brief transform an otbImage into an otbVectorImage by mapping scalar value into rainbow value.
     *
     *  Grayscale image is mapped to a color image where blue represents
     * small values and red represents big values.
     *
     * \example BasicFilters/DEMToRainbowExample.cxx
     *
     */
    template<class TInputImage, class TOutputImage>
        class ScalarToRainbowVectorImageFilter : public itk::UnaryFunctorImageFilter<TInputImage, TOutputImage,
                Functor::ScalarToRainbowVariableLengthVectorPixelFunctor<typename TInputImage::PixelType, typename TOutputImage::PixelType> >
        {
        public:
            /** Standard class typedefs. */
            typedef ScalarToRainbowVectorImageFilter Self;
            typedef itk::UnaryFunctorImageFilter<TInputImage, TOutputImage,
                    Functor::ScalarToRainbowVariableLengthVectorPixelFunctor<typename TInputImage::PixelType,
                            typename TOutputImage::PixelType> > Superclass;
            typedef itk::SmartPointer<Self> Pointer;
            typedef itk::SmartPointer<const Self> ConstPointer;

            typedef typename TInputImage::InternalPixelType ScalarType;

            /** Method for creation through the object factory. */
            itkNewMacro(Self)

            /** Run-time type information (and related methods). */
            itkTypeMacro(ScalarToRainbowVectorImageFilter, itk::UnaryFunctorImageFilter)

            /** Set the input minimum  and maximum to be mapped to red */
            void
            SetMinimumMaximum(const ScalarType & min, const ScalarType & max)
            {
                this->GetFunctor().SetMinimumMaximum(min, max);
            }

        protected:
            ScalarToRainbowVectorImageFilter();
            virtual
            ~ScalarToRainbowVectorImageFilter()
            {
            }

            virtual void
            GenerateOutputInformation();

        private:
            ScalarToRainbowVectorImageFilter(const Self&); //purposely not implemented
            void
            operator=(const Self&); //purposely not implemented
        };

} // end namespace vns

#ifndef VNS_MANUAL_INSTANTIATION
#include "vnsScalarToRainbowVectorImageFilter.txx"
#endif

#endif
