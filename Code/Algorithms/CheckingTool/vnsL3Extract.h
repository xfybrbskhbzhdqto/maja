// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 5-1-0 : DM : LAIG-DM-MAC-1769-CNES : 6 juillet 2016 : Implémentation écart type checktools     *
 * VERSION : 5-1-0 : FA : LAIG-FA-MAC-145739-CS : 27 juin 2016 : Audit code - Supp de la macro ITK_EXPORT   *
 * VERSION : 1-0-0-3 : DM : 251 : 17 aout 2011 : Modif. pour prise en compte DM 251 Checktools dans SMIGS     *
 *                                                                                                          *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 17 sept. 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsL3Extract_h
#define __vnsL3Extract_h

#include "vnsExtractBase.h"
#include "itkImageRegionConstIterator.h"
#include "otbMultiChannelExtractROI.h"
#include "otbStreamingStatisticsImageFilter.h"
#include "vnsStreamingMaskConditionalStatisticsVectorImageFilter.h"
#include "otbExtractROI.h"
#include "otbImageToVectorImageCastFilter.h"
#include "vnsMacro.h"

namespace vns
{
/** \class  L3Extract
 * \brief Extract informations from L3 product.
 * These informations are computed for a list of Pixels (use SetListPixelLineId and SetListPixelColId)
 * and are computed on a neighborhood (if any).
 *  -SRE Means
 *  -FRE Means
 *  -PXD Means
 *  -SAT Value
 *
 * \author CS Systemes d'Information
 *
 * \sa ProcessObject
 *
 * \ingroup L2
 * \ingroup L3
 * \ingroup Checktool
 *
 */
template <class TInputReflectanceImage, class TInputMaskImage>
class L3Extract : public ExtractBase
{
public:
    /** Standard class typedefs. */
    typedef L3Extract                         Self;
    typedef ExtractBase          Superclass;
    typedef itk::SmartPointer<Self>           Pointer;
    typedef itk::SmartPointer<const Self>     ConstPointer;

    /** Some convenient typedefs. */
    typedef TInputReflectanceImage                           InputReflectanceImageType;
    typedef typename InputReflectanceImageType::ConstPointer InputReflectanceImageConstPointer;
    typedef typename InputReflectanceImageType::Pointer      InputReflectanceImagePointer;
    typedef typename InputReflectanceImageType::PixelType    InputReflectanceImagePixelType;
    typedef typename InputReflectanceImageType::InternalPixelType    InputReflectanceImageInternalPixelType;
    typedef typename InputReflectanceImageType::IndexType    IndexType;
    typedef typename InputReflectanceImageType::SizeType      SizeType;   
    typedef typename InputReflectanceImageType::RegionType    RegionType;

    typedef TInputMaskImage InputMaskImageType;
    typedef typename InputMaskImageType::ConstPointer InputMaskImageConstPointer;
    typedef typename InputMaskImageType::Pointer InputMaskImagePointer;
    typedef typename InputMaskImageType::PixelType    InputMaskImagePixelType;
    typedef typename InputMaskImageType::InternalPixelType    InputMaskImageInternalPixelType;

    /** Internam typedef to cast amount images */
    typedef otb::VectorImage<InputMaskImagePixelType> MaskVectorImageType;

    typedef otb::ImageToVectorImageCastFilter<InputMaskImageType, MaskVectorImageType> CasterType;

    typedef otb::MultiChannelExtractROI<InputReflectanceImageInternalPixelType,InputReflectanceImageInternalPixelType>     MultiChannelExtractROIType;
    typedef typename MultiChannelExtractROIType::Pointer MultiChannelExtractROIPointer;

    typedef otb::ExtractROI<InputMaskImagePixelType,InputMaskImagePixelType>    ExtractROIMaskImageType;
    typedef typename ExtractROIMaskImageType::Pointer ExtractROIMaskImagePointer;

    typedef otb::StreamingStatisticsImageFilter<InputMaskImageType> StatisticsMaskFilterType;
    typedef typename StatisticsMaskFilterType::Pointer StatisticsMaskFilterPointer;

    typedef StreamingMaskConditionalStatisticsVectorImageFilter<MaskVectorImageType, InputMaskImageType> StatisticsVectorMaskFilterType;
    typedef typename StatisticsVectorMaskFilterType::Pointer StatisticsVectorMaskFilterPointer;

    typedef StreamingMaskConditionalStatisticsVectorImageFilter<InputReflectanceImageType, InputMaskImageType> StatisticsImageFilterType;
    typedef typename StatisticsImageFilterType::Pointer StatisticsImageFilterPointer;


    /** Type macro */
    itkNewMacro(Self);
    /** Creation through object factory macro */
    itkTypeMacro(L3Extract, ExtractBase );

    /** Set/Get Macro */
    itkSetObjectMacro(SREImage, InputReflectanceImageType);
    itkSetObjectMacro(FREImage, InputReflectanceImageType);
    itkSetObjectMacro(PXDImage, InputMaskImageType);
    itkSetObjectMacro(SATImage, InputMaskImageType);
    itkSetMacro(UseFRE, bool);
    itkGetMacro(UseFRE, bool);
    itkSetMacro(NoData, RealNoDataType);
    itkGetMacro(NoData, RealNoDataType);
    //Get output vector
    vnsGetConstReferenceMacro(SREMeans, InputReflectanceImagePixelType );
    vnsGetConstReferenceMacro(FREMeans, InputReflectanceImagePixelType );
    vnsGetConstReferenceMacro(SREStDv, InputReflectanceImagePixelType );
    vnsGetConstReferenceMacro(FREStDv, InputReflectanceImagePixelType );
    vnsGetConstReferenceMacro(UsefulPixelsPercentage, InputReflectanceImagePixelType);
    vnsGetConstReferenceMacro(ValidPixelsPercentage, InputReflectanceImagePixelType);
    vnsGetConstReferenceMacro(PXDMeans, double );
    vnsGetConstReferenceMacro(PXDStDv, double );

    vnsGetConstReferenceMacro(SATVal, InputMaskImagePixelType );
    /** Get if the SRE statistics of the band is valid */
    itkGetConstMacro(SREStatIsValid,bool)
    /** Get if the FRE statistics of the band is valid */
    itkGetConstMacro(FREStatIsValid,bool)
    

protected:
    /** Constructor */
    L3Extract();
    /** Destructor */
    virtual ~L3Extract();
    /** PrintSelf method */
    virtual void PrintSelf(std::ostream& os, itk::Indent indent) const;
    virtual void ComputeIndexStat(IndexType & id);

private:
    L3Extract(const Self&); //purposely not implemented
    void operator=(const Self&); //purposely not implemented

    //input Images
    InputMaskImagePointer m_SATImage;
    InputMaskImagePointer m_PXDImage;
    InputReflectanceImagePointer m_SREImage;
    InputReflectanceImagePointer m_FREImage;
    //No Data
    RealNoDataType m_NoData;

    //outputs vector
    InputReflectanceImagePixelType m_SREMeans;
    InputReflectanceImagePixelType m_FREMeans;
    InputReflectanceImagePixelType m_SREStDv;
    InputReflectanceImagePixelType m_FREStDv;
    InputReflectanceImagePixelType m_UsefulPixelsPercentage;
    InputReflectanceImagePixelType m_ValidPixelsPercentage;
    double m_PXDMeans;
    double m_PXDStDv;
    InputMaskImagePixelType m_SATVal;
    bool m_UseFRE;

    /** Check if the SRE statistics of the band is valid */
    bool m_SREStatIsValid;
    /** Check if the FRE statistics of the band is valid */
    bool m_FREStatIsValid;
    /** Check if the PXD statistics of the band is valid */
    bool m_PXDStatIsValid;
};

} // End namespace vns
#ifndef VNS_MANUAL_INSTANTIATION
#include "vnsL3Extract.txx"
#endif

#endif /* __vnsL3Extract_h */
