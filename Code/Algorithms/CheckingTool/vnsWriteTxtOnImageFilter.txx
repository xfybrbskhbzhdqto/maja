// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 1.0.0 : FA : LAIG-FA-MAC-148399-CS : 03 novembre 2016 : Audit code - Correction qualite        *
 * VERSION : 4-0-0 : FA : LAIG-FA-MAC-117040-CS : 13 mars 2014 : Modifications mineures                     *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 27 sept. 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsWriteTxtOnImageFilter_txx
#define __vnsWriteTxtOnImageFilter_txx

#include "vnsWriteTxtOnImageFilter.h"

namespace vns
{

    /** Constructor */
    template<class TInputImage, class TOutputImage>
        WriteTxtOnImageFilter<TInputImage, TOutputImage>::WriteTxtOnImageFilter() :
                m_FontSize(20), m_Text("TEXT_NOT_INITIALIZED")
        {
            //instantiate filters
            m_ConvertTxtToImageFilter = ConvertTxtToImageFilterType::New();
            m_PasteImageFilter = PasteImageFilterType::New();
            m_Input = InputImageType::New();
        }

    /** Destructor */
    template<class TInputImage, class TOutputImage>
        WriteTxtOnImageFilter<TInputImage, TOutputImage>::~WriteTxtOnImageFilter()
        {
        }

    /**GenerateData */
    template<class TInputImage, class TOutputImage>
        void
        WriteTxtOnImageFilter<TInputImage, TOutputImage>::UpdateData()
        {
            m_Input->UpdateOutputInformation();

            // Set the parameters to the Convert TXX filter
            m_ConvertTxtToImageFilter->SetFontSize(m_FontSize);
            m_ConvertTxtToImageFilter->SetText(m_Text);
            m_ConvertTxtToImageFilter->Convert();
            // Initialize the position of the corner
            const int offsetCorner(10);

            if ((m_Input->GetLargestPossibleRegion().GetSize()[0]
                    < m_ConvertTxtToImageFilter->GetOutputImage()->GetLargestPossibleRegion().GetSize()[0] + offsetCorner)
                    || (m_Input->GetLargestPossibleRegion().GetSize()[1]
                            < m_ConvertTxtToImageFilter->GetOutputImage()->GetLargestPossibleRegion().GetSize()[1] + offsetCorner))
            {
                vnsStaticExceptionBusinessMacro(
                        "Label image ("<<m_ConvertTxtToImageFilter->GetOutputImage()->GetLargestPossibleRegion().GetSize()<<") with Offset corner "<<offsetCorner<<" bigger than input one "<<m_Input->GetLargestPossibleRegion().GetSize()<<"). Please, change input or mower font size (here " <<m_FontSize<<").");
            }

            vnsLogSuperDebugMacro(
                    "WriteTxtOnImageFilter<TInputImage, TOutputImage>::UpdateData m_Input->GetLargestPossibleRegion: "<<m_Input->GetLargestPossibleRegion())
            vnsLogSuperDebugMacro(
                    "WriteTxtOnImageFilter<TInputImage, TOutputImage>::UpdateData m_ConvertTxtToImageFilter->GetOutputImage()->GetLargestPossibleRegion(): "<<m_ConvertTxtToImageFilter->GetOutputImage()->GetLargestPossibleRegion())

            //write in upper right corner. We take a marge of 10 pixels
            m_DestinationIndex[0] = m_Input->GetLargestPossibleRegion().GetSize()[0]
                    - m_ConvertTxtToImageFilter->GetOutputImage()->GetLargestPossibleRegion().GetSize()[0] - offsetCorner;
            m_DestinationIndex[1] = m_Input->GetLargestPossibleRegion().GetIndex()[1] + offsetCorner;

            vnsLogSuperDebugMacro("m_DestinationIndex: "<<m_DestinationIndex)
            vnsLogSuperDebugMacro("Start m_PasteImageFilter ...")
            // Set the parameters to the Past filter
            // 4.2 : Modify the tolerance, because images not same origin
            m_PasteImageFilter->SetCoordinateTolerance(10000000.);
            m_PasteImageFilter->SetDirectionTolerance(10000000.);
            m_PasteImageFilter->SetDestinationImage(m_Input);
            m_PasteImageFilter->SetSourceImage(m_ConvertTxtToImageFilter->GetOutputImage());
            m_PasteImageFilter->SetSourceRegion(m_ConvertTxtToImageFilter->GetOutputImage()->GetLargestPossibleRegion());
            m_PasteImageFilter->SetDestinationIndex(m_DestinationIndex);

            vnsLogSuperDebugMacro("Start m_PasteImageFilter->UpdateOutputInformation() ...")

            m_PasteImageFilter->UpdateOutputInformation();

            vnsLogSuperDebugMacro("WriteTxtOnImageFilter UpdateData() done.")
        }

} // namespace venus
#endif /* __vnsWriteTxtOnImageFilter_txx */
