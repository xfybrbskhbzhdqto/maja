// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 3-2-0 : FA : LAIG-FA-MAC-701-CNES : 5 decembre 2013 : Suite correction Spacing dans le filtre  *
 *                  LinearSubSampleImageFilter, impact sur la sortie du filtre de GradientMagnitude (car    *
 *                  utilise le Spacing dans le coefficient). Solution : force a UseImageSpacingOff pour     *
 *                  m_GradientMagnitudeImageFilter                                                          *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 31 août 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsL2QLExtractEdge_txx
#define __vnsL2QLExtractEdge_txx

#include "vnsL2QLExtractEdge.h"
//#include "vnsCaching.h"

namespace vns
{

/** Constructor */
template <class TInputImage, class TOutputImage>
L2QLExtractEdge<TInputImage, TOutputImage>
::L2QLExtractEdge()
{
    /** Create filters */
    m_ShiftScaleImageFilter = ShiftScaleImageFilterType::New();
    //default values
    m_ShiftScaleImageFilter->SetShift(0);
    m_ShiftScaleImageFilter->SetScale(255);
    m_GradientMagnitudeImageFilter = GradientMagnitudeImageFilterType::New();
    // VERSION : 3-2-0 : FA : LAIG-FA-MAC-701-CNES : Force Disable Spacing
    m_GradientMagnitudeImageFilter->SetUseImageSpacingOff();

}

/** Destructor */
template <class TInputImage, class TOutputImage>
L2QLExtractEdge<TInputImage, TOutputImage>
::~L2QLExtractEdge()
{
}


template <class TInputImage, class TOutputImage>
void
L2QLExtractEdge<TInputImage, TOutputImage>
::UpdateData()
{
    m_GradientMagnitudeImageFilter->SetInput(m_ShiftScaleImageFilter->GetOutput());
}


/** PrintSelf method */
template<class TInputImage, class TOutputImage>
void 
L2QLExtractEdge<TInputImage, TOutputImage>
::PrintSelf(std::ostream& os, itk::Indent indent) const
{
    this->Superclass::PrintSelf(os, indent);
}

} // End namespace vns

#endif /* __vnsL2QLExtractEdge_txx */
