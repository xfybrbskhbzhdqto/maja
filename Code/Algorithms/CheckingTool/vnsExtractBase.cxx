// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 5.1.0 : DM : LAIG-DM-MAC-1769-CNES : 9 aout 2016 : Modification des checktools                 *
 * VERSION : 1-0-0-3 : DM : 251 : 17 aout 2011 : Modif. pour prise en compte DM 251 Checktools dans SMIGS     *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 14 sept. 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/

#include "vnsExtractBase.h"
#include "vnsMacro.h"

namespace vns
{
    //Const value
    const double ExtractBase::ERROR_VALUE = -999.0;

    /** Constructor */
    ExtractBase::ExtractBase() :  m_Radius(0)
    {
        m_Point.Fill(0);
    }

    /** ComputeIndexStat : compute stats for one index*/
    void
    ExtractBase::ComputeIndexStat(IndexType & /*id*/)
    {
        vnsExceptionBusinessMacro("Virtual method, must be overloaded" )
    }

    /** ComputeStats */
    void
    ExtractBase::ComputeStats(void)
    {
        this->ComputeIndexStat(m_Point);
    }

    /** PrintSelf method */
    void
    ExtractBase::PrintSelf(std::ostream& os, itk::Indent indent) const
    {
        this->Superclass::PrintSelf(os, indent);
    }

} // End namespace vns

