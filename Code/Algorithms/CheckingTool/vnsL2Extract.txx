// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 1.0.0 : FA : LAIG-FA-MAC-148399-CS : 03 novembre 2016 : Audit code - Correction qualite        *
 * VERSION : 5-1-0 : DM : LAIG-DM-MAC-1755-CNES : 6 juillet 2016 : Implémentation pourcentages utils/valids *
 * VERSION : 5-1-0 : DM : LAIG-DM-MAC-1769-CNES : 6 juillet 2016 : Implémentation écart type checktools     *
 * VERSION : 3-0-0 : FA : LAIG-FA-MAC-371-CNES : 05 octobre 2012 : Correction qualite :  TxCom et Vg        *
 * VERSION : 1-0-0-3 : DM : 251 : 17 aout 2011 : Modif. pour prise en compte DM 251 Checktools dans SMIGS     *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 16 sept. 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsL2Extract_txx
#define __vnsL2Extract_txx

#include "otbMath.h"
#include "vnsL2Extract.h"

namespace vns
{

    /** Constructor */
    template<class TInputReflectanceImage, class TInputAmountImage, class TInputMaskCLDImage, class TInputMaskImage>
        L2Extract<TInputReflectanceImage, TInputAmountImage, TInputMaskCLDImage, TInputMaskImage>::L2Extract() :
        m_NoData(0),m_VAPNoData(0),m_AOTNoData(0),m_VAPUseFulPixelsPercentage(0), m_AOTUseFulPixelsPercentage(0), m_VAPValidPixelsPercentage(0),
        m_AOTValidPixelsPercentage(0), m_UseFRE(true)
        {
            //default values
            m_VapRadius = 0;
            m_AOTRadius = 0;
            m_FREMeans.Fill(0);

            //Create input images pointer
            m_VAPImage = InputAmountImageType::New();
            m_AOTImage = InputAmountImageType::New();
            m_EDGImage = InputMaskImageType::New();
            m_WATImage = InputMaskImageType::New();
            m_SREImage = InputReflectanceImageType::New();
            m_FREImage = InputReflectanceImageType::New();
            m_CLDImage = InputMaskCLDImageType::New();

            m_SREStatIsValid = false;
            m_FREStatIsValid = false;
            m_VAPStatIsValid = false;
            m_AOTStatIsValid = false;
        }

    template<class TInputReflectanceImage, class TInputAmountImage, class TInputMaskCLDImage, class TInputMaskImage>
        void
        L2Extract<TInputReflectanceImage, TInputAmountImage, TInputMaskCLDImage, TInputMaskImage>::ComputeIndexStat(IndexType & id)
        {
            // Update output information image
            m_VAPImage->UpdateOutputInformation();
            m_AOTImage->UpdateOutputInformation();
            m_EDGImage->UpdateOutputInformation();
            m_WATImage->UpdateOutputInformation();
            m_SREImage->UpdateOutputInformation();
            m_CLDImage->UpdateOutputInformation();

            //Get Radius
            unsigned int radius = this->GetRadius();

            RegionType l_Region = m_EDGImage->GetLargestPossibleRegion();

            // Check that all the imput images have the same size
            if ((l_Region != m_AOTImage->GetLargestPossibleRegion()) || (l_Region != m_VAPImage->GetLargestPossibleRegion()) || (l_Region
                    != m_CLDImage->GetLargestPossibleRegion()) || (l_Region != m_WATImage->GetLargestPossibleRegion()) || (l_Region
                    != m_SREImage->GetLargestPossibleRegion()))
            {
                vnsExceptionBusinessMacro("Input images size mismatch");
            }
            // If the surface reflectance image with slope correction is used
            if (m_UseFRE == true)
            {
                m_FREImage->UpdateOutputInformation();
                if (l_Region != m_FREImage->GetLargestPossibleRegion())
                {
                    vnsExceptionBusinessMacro("Input images size mismatch");
                }
            }

            // Compute statistics for the SRE image
            vnsLogDebugMacro("Compute statistics for the SRE image...")
            m_SREStatIsValid = this->computeROIStats<InputReflectanceImageType>(radius, id, m_SREImage, m_SREMeans,
                    m_SREStDv, m_UseFulPixelsPercentage, m_ValidPixelsPercentage,
                    m_CloudValR1, m_WaterValR1, m_EdgeValR1, m_NoData);
            vnsLogDebugMacro("Compute statistics for the SRE image done. ("<<Utilities::BoolToLowerString(m_SREStatIsValid)<<").")


            // If the FRE image is available
            if (m_UseFRE == true)
            {
                InputReflectanceImagePixelType l_UsefullPixelPerc;
                InputReflectanceImagePixelType l_ValidPixelPerc;
                // Extract the region in the "flat" surface reflectance image
                m_FREImage->UpdateOutputInformation();

                // Check if the region is inside the input image largest possible region
                if (l_Region != m_FREImage->GetLargestPossibleRegion())
                {
                    vnsExceptionBusinessMacro("Input image size mismatch");
                }

                // Compute statistics for the FRE image
                vnsLogDebugMacro("Compute statistics for the FRE image...")
                m_FREStatIsValid = this->computeROIStats<InputReflectanceImageType>(radius, id, m_FREImage, m_FREMeans,
                        m_FREStDv, l_UsefullPixelPerc, l_ValidPixelPerc,
                        m_CloudValR1, m_WaterValR1, m_EdgeValR1, m_NoData);
                vnsLogDebugMacro("Compute statistics for the FRE image done. ("<<Utilities::BoolToLowerString(m_FREStatIsValid)<<").")
            }
            // If the surface reflectance image with slope correction is not used
            else
            {
                InputReflectanceImagePixelType res;
                // Get the SRE NumberOfComponentsPerPixel to initialize the FRE pixel size
                res.SetSize(m_SREImage->GetNumberOfComponentsPerPixel());
                res.Fill(0.);
                m_FREMeans = res;
                m_FREStDv = res;
                m_FREStatIsValid = false;
            }

            typename InputAmountImageCastType::PixelType l_Means;
            l_Means.Fill(ERROR_VALUE);
            typename InputAmountImageCastType::PixelType l_StDv;
            l_StDv.Fill(0);
            typename InputAmountImageCastType::PixelType l_UsefullPixelPerc;
            l_UsefullPixelPerc.Fill(0);
            typename InputAmountImageCastType::PixelType l_ValidPixelPerc;
            l_ValidPixelPerc.Fill(0);

            // Cast into VectorImage to use the vns::StreamingMaskConditionalStatisticsVectorImageFilter
            typename CasterType::Pointer l_VAPCaster = CasterType::New();
            l_VAPCaster->SetInput(m_VAPImage);
            l_VAPCaster->UpdateOutputInformation();

            // Compute statistics for the VAP image
            vnsLogDebugMacro("Compute statistics for the VAP image...")
            m_VAPStatIsValid =  this->computeROIStats<InputAmountImageCastType>(m_VapRadius, id, l_VAPCaster->GetOutput(), l_Means,
                    l_StDv,l_UsefullPixelPerc, l_ValidPixelPerc,
                    m_CloudValR2, m_WaterValR2, m_EdgeValR2, m_VAPNoData);
            vnsLogDebugMacro("Compute statistics for the VAP image done ("<<Utilities::BoolToLowerString(m_VAPStatIsValid)<<").")

            if (m_VAPStatIsValid == true)
            {
                  // VAP image is an otb::image and not a vector image
                  m_VAPMeans = l_Means[0];
                  m_VAPStDv = l_StDv[0];
                  m_VAPUseFulPixelsPercentage = l_UsefullPixelPerc[0];
                  m_VAPValidPixelsPercentage = l_ValidPixelPerc[0];
            }
            else
            {
                m_VAPMeans = ERROR_VALUE;
                m_VAPStDv = 0;
                m_VAPUseFulPixelsPercentage = 0;
                m_VAPValidPixelsPercentage = 0;
            }

          // Cast into VectorImage the vns::StreamingMaskConditionalStatisticsVectorImageFilter
            typename CasterType::Pointer l_AOTCaster = CasterType::New();
            l_AOTCaster->SetInput(m_AOTImage);
            l_AOTCaster->UpdateOutputInformation();

            //Restore to default
            l_Means.Fill(ERROR_VALUE);
            l_StDv.Fill(0);
            l_UsefullPixelPerc.Fill(0);
            l_ValidPixelPerc.Fill(0);

            // Compute statistics for the AOT image
            vnsLogDebugMacro("Compute statistics for the AOT image...")
            m_AOTStatIsValid =  this->computeROIStats<InputAmountImageCastType>(m_AOTRadius, id, l_AOTCaster->GetOutput(), l_Means,
                    l_StDv,l_UsefullPixelPerc, l_ValidPixelPerc,
                    m_CloudValR3, m_WaterValR3, m_EdgeValR3, m_AOTNoData);
            vnsLogDebugMacro("Compute statistics for the AOT image done ("<<Utilities::BoolToLowerString(m_AOTStatIsValid)<<").")

            if (m_AOTStatIsValid == true)
            {
                // VAP image is an otb::image and not a vector image
                m_AOTMeans = l_Means[0];
                m_AOTStDv = l_StDv[0];
                m_AOTUseFulPixelsPercentage = l_UsefullPixelPerc[0];
                m_AOTValidPixelsPercentage = l_ValidPixelPerc[0];
            }
            else
            {
                m_AOTMeans = ERROR_VALUE;
                m_AOTStDv = 0;
                m_AOTUseFulPixelsPercentage = 0;
                m_AOTValidPixelsPercentage = 0;
            }

        }

    /** PrintSelf method */
    template<class TInputReflectanceImage, class TInputAmountImage, class TInputMaskCLDImage, class TInputMaskImage>
        void
        L2Extract<TInputReflectanceImage, TInputAmountImage, TInputMaskCLDImage, TInputMaskImage>::PrintSelf(std::ostream& os,
                itk::Indent indent) const
        {
            this->Superclass::PrintSelf(os, indent);
        }

} // End namespace vns

#endif /* __vnsL2Extract_txx */
