// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 5-1-0 : FA : LAIG-FA-MAC-145739-CS : 27 juin 2016 : Audit code - Supp de la macro ITK_EXPORT   *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 22 sept. 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsPasteImageFilter_h
#define __vnsPasteImageFilter_h

#include "itkInPlaceImageFilter.h"
#include "itkSmartPointer.h"
#include "vnsMacro.h"

namespace vns
{

    /** \class PasteImageFilter
     * \brief Paste a vector image into another vector image.
     *
     * PasteImageFilter allows you to take a section of one image and
     * paste into another image.  The SetDestinationIndex() method
     * prescribes where in the first input to start pasting data from the
     * second input.  The SetSourceRegion method prescribes the section of
     * the second image to paste into the first. If the output requested
     * region does not have include the SourceRegion after it has been
     * repositioned to DestinationIndex, then the output will just be
     * a copy of the input.
     *
     * The two inputs and output image will have the same pixel type.
     *
     * \ingroup GeometricTransforms
     */
    template<class TInputImage, class TSourceImage = TInputImage, class TOutputImage = TInputImage>
        class PasteImageFilter : public itk::InPlaceImageFilter<TInputImage, TOutputImage>
        {
        public:
            /** Standard class typedefs. */
            typedef PasteImageFilter Self;
            typedef itk::InPlaceImageFilter<TInputImage, TOutputImage> Superclass;
            typedef itk::SmartPointer<Self> Pointer;
            typedef itk::SmartPointer<const Self> ConstPointer;

            /** Method for creation through the object factory. */
            itkNewMacro(Self)
            ;

            /** Run-time type information (and related methods). */
            itkTypeMacro(PasteImageFilter, InPlaceImageFilter)
            ;

            /** Typedefs from Superclass */
            typedef typename Superclass::InputImagePointer InputImagePointer;
            typedef typename Superclass::OutputImagePointer OutputImagePointer;

            /** Typedef to describe the output and input image region types. */
            typedef TInputImage InputImageType;
            typedef TOutputImage OutputImageType;
            typedef TSourceImage SourceImageType;
            typedef typename OutputImageType::RegionType OutputImageRegionType;
            typedef typename InputImageType::RegionType InputImageRegionType;
            typedef typename SourceImageType::RegionType SourceImageRegionType;

            typedef typename SourceImageType::Pointer SourceImagePointer;
            typedef typename SourceImageType::ConstPointer SourceImageConstPointer;

            /** Typedef to describe the type of pixel. */
            typedef typename OutputImageType::PixelType OutputImagePixelType;
            typedef typename OutputImageType::InternalPixelType OutputImageInternalPixelType;
            typedef typename InputImageType::PixelType InputImagePixelType;
            typedef typename SourceImageType::PixelType SourceImagePixelType;
            typedef typename SourceImageType::InternalPixelType SourceImageInternalPixelType;

            /** Typedef to describe the output and input image index and size types. */
            typedef typename OutputImageType::IndexType OutputImageIndexType;
            typedef typename OutputImageType::SizeType OutputImageSizeType;
            typedef typename InputImageType::IndexType InputImageIndexType;
            typedef typename InputImageType::SizeType InputImageSizeType;
            typedef typename SourceImageType::IndexType SourceImageIndexType;
            typedef typename SourceImageType::SizeType SourceImageSizeType;

            /** ImageDimension enumeration */
            itkStaticConstMacro(InputImageDimension, unsigned int,
                    InputImageType::ImageDimension);
            itkStaticConstMacro(OutputImageDimension, unsigned int,
                    OutputImageType::ImageDimension);
            itkStaticConstMacro(SourceImageDimension, unsigned int,
                    SourceImageType::ImageDimension);

            /** Set/Get the destination index (where in the first input the second
             * input will be pasted. */
            itkSetMacro(DestinationIndex, InputImageIndexType);
            itkGetConstMacro(DestinationIndex, InputImageIndexType);

            /** Set/Get the source region (what part of the second input will be
             * pasted. */
            itkSetMacro(SourceRegion, SourceImageRegionType);
            itkGetConstMacro(SourceRegion, SourceImageRegionType);

            /** Set/Get the "destination" image.  This is the image that will be
             * obscured by the paste operation. */
            vnsSetGetImageMacro( Destination, InputImageType, 0)

            /** Set/Get the "source" image.  This is the image that will be
             * pasted over the destination image. */
            vnsSetGetImageMacro( Source, SourceImageType, 1)

            /** PasteImageFilter needs to set the input requested regions for its
             * inputs.  The first input's requested region will be set to match
             * the output requested region.  The second input's requested region
             * will be set to the value of the m_SourceRegion ivar.  Note that
             * if the output requested region is a portion of the image that
             * is outside the DestinationIndex + size of the source region,
             * then the first input is copied to the output.
             *
             * \sa ProcessObject::GenerateInputRequestedRegion() */
            virtual void GenerateInputRequestedRegion();

        protected:
            PasteImageFilter();
            virtual ~PasteImageFilter()
            {};
            void PrintSelf(std::ostream& os, itk::Indent indent) const;

            /** PasteImageFilter can be implemented as a multithreaded filter.
             * Therefore, this implementation provides a ThreadedGenerateData()
             * routine which is called for each processing thread. The output
             * image data is allocated automatically by the superclass prior to
             * calling ThreadedGenerateData().  ThreadedGenerateData can only
             * write to the portion of the output image specified by the
             * parameter "outputRegionForThread"
             * \sa ImageToImageFilter::ThreadedGenerateData(),
             *     ImageToImageFilter::GenerateData()  */
            void ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread,
                    itk::ThreadIdType threadId );

            SourceImageRegionType m_SourceRegion;
            InputImageIndexType m_DestinationIndex;

        private:
            PasteImageFilter(const Self&); //purposely not implemented
                void operator=(const Self&);//purposely not implemented

                SourceImageInternalPixelType m_BackgroundValue;

            };

        }
        // end namespace vns

#ifndef VNS_MANUAL_INSTANTIATION
#include "vnsPasteImageFilter.txx"
#endif

#endif
