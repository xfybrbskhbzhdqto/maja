/*
 * Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
/************************************************************************************************************
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              *
 *                                                                                                          *
 ************************************************************************************************************
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 15 nov. 2017 : Creation
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
#include "vnsScalarToRainbowVectorImageFilter.h"

#include <string>

namespace vns
{

namespace Wrapper
{

using namespace otb::Wrapper;

class ColorScale : public Application
{
public:
  /** Standard class typedefs. */
  typedef ColorScale                    Self;
  typedef otb::Wrapper::Application     Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Standard macro */
  itkNewMacro(Self);
  itkTypeMacro(ColorScale, otb::Wrapper::Application);

  typedef UInt8VectorImageType VectorMaskType;
  typedef DoubleImageType      ImageType;
  // typedef DoubleImageType DoubleImage;

  typedef vns::ScalarToRainbowVectorImageFilter<ImageType, VectorMaskType> FilterType;
  typedef typename FilterType::Pointer                                     FilterPointerType;


private:
  void DoInit()
  {
    SetName("ColorScale");
    SetDescription("ColorScale");
    Loggers::GetInstance()->Initialize(GetName());
    // Documentation
    SetDocLongDescription("Grayscale image is mapped to a color image where blue represents small values and red represents big values");
    SetDocLimitations("None");
    SetDocAuthors("MAJA-Team");
    SetDocSeeAlso("MAJA Doc");

    AddDocTag("Mask");

    AddParameter(ParameterType_InputImage, "in", "input vectorimage");
    AddParameter(ParameterType_OutputImage, "out", "output vectorimage");
    SetParameterDescription("out", "output imagvectorimagee");
    AddParameter(ParameterType_Float, "max", "maximum");
    AddParameter(ParameterType_Float, "min", "minimum");

    AddRAMParameter("ram");
    SetDefaultParameterInt("ram", 2048);
  }

  void DoUpdateParameters()
  {
  }


  void DoExecute()
  {
    // Init filters
    filter = FilterType::New();

    filter->SetInput(this->GetParameterDoubleImage("in"));
    filter->SetMinimumMaximum(this->GetParameterFloat("min"), this->GetParameterFloat("max"));

    SetParameterOutputImage<VectorMaskType>("out", filter->GetOutput());
  }


  /** Filters declaration */
  FilterPointerType filter;
};

} // namespace Wrapper
} // namespace vns

OTB_APPLICATION_EXPORT(vns::Wrapper::ColorScale)
