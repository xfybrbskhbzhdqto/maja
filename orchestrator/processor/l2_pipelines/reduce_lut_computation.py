# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
###################################################################################################
#
#                        o     o
#                        oo   oo   oo        o   oo        ,-.
#                        o o o o  o  o       o  o  o       \_/
#                        o  o  o o    o      o o    o     {|||)<
#                        o     o oooooo      o oooooo      / \
#                        o     o o    o o    o o    o      `-^
#                        o     o o    o  oooo  o    o
#
###################################################################################################

orchestrator.processor.base_processor -- shortdesc

orchestrator.processor.base_processor is the base of all processors

It defines method mandatory for a processor


###################################################################################################
"""
from orchestrator.common.logger.maja_logging import configure_logger
from orchestrator.cots.otb.otb_app_handler import OtbAppHandler
from orchestrator.modules.maja_module import MajaModule
from orchestrator.common.maja_exceptions import *
import orchestrator.common.xml_tools as xml_tools
from orchestrator.common.interfaces.maja_xml_app_lutmap import *
import orchestrator.common.interfaces.maja_xml_app_lut as maja_xml_app_lut
from orchestrator.common.constants import DirectionalCorrection
from orchestrator.cots.otb.otb_pipeline_manager import OtbPipelineManager
import os
import io
import numpy as np

LOGGER = configure_logger(__name__)


class MajaReduceLutComputation(MajaModule):
    """
    classdocs
    """

    NAME = "ReduceLut"

    def __init__(self):
        """
        Constructor
        """
        super(MajaReduceLutComputation, self).__init__()
        self.in_keys_to_check = [
            "Params.Caching",
            "Params.DIRCORModel",
            "AppHandler",
            "Plugin",
            "L1Reader",
            "L2COMM",
            "L1Info",
            "L2TOCR",
            "DIRCOR",
        ]
        self.out_keys_to_check = []
        self.out_keys_provided = ["cr_lut"]
        self._lut_pipeline = OtbPipelineManager()

    def find_missing_detector(self, listDTF, key, adjacent_dtf_nb):
        nb_bands = 13
        wrongDTFFound = False
        adjacentDTFFound = False
        for i in range(0, nb_bands):
            if [
                elem for elem in listDTF[i].values() if "D" + str(key) + ".tif" in elem
            ]:
                dft_wrong = [
                    elem
                    for elem in listDTF[i].values()
                    if "D" + str(key) + ".tif" in elem
                ][0]
                wrongDTFFound = True

            if [
                elem for elem in listDTF[i].values() if "D" + str(key) + ".tif" in elem
            ]:
                dft_adjacent = [
                    elem
                    for elem in listDTF[i].values()
                    if "D" + str(adjacent_dtf_nb) + ".tif" in elem
                ][0]
                adjacentDTFFound = True

            if wrongDTFFound == True and adjacentDTFFound == True:
                return dft_wrong, dft_adjacent

        LOGGER.warning(
            "Missing detector file in L1 masks list : maskName-R1-D-{}".format(key)
            + ".tif"
        )
        return "", ""

    def run(self, dict_of_input, dict_of_output):
        LOGGER.info("Lut computation start")
        l_writeL2 = dict_of_input.get("Params").get("WriteL2ProductToL2Resolution")
        viewing_zenith = dict_of_input.get(
            "L1Info"
        ).ListOfViewingZenithAnglesPerBandAtL2CoarseResolution
        viewing_azimuth = dict_of_input.get(
            "L1Info"
        ).ListOfViewingAzimuthAnglesPerBandAtL2CoarseResolution

        l_dircormodel = dict_of_input.get("Params").get("DIRCORModel")

        # CR Lut generation
        rlc_working = (
            dict_of_input.get("AppHandler")
            .get_directory_manager()
            .get_temporary_directory("ReduceLutProc_", do_always_remove=True)
        )

        if l_dircormodel != DirectionalCorrection.DEACTIVATED:
            # Directional Correction
            param_dircorr = {
                "solar.zenith": float(
                    dict_of_input.get("L1Info").SolarAngle["sun_zenith_angle"]
                ),
                "solar.azimuth": float(
                    dict_of_input.get("L1Info").SolarAngle["sun_azimuth_angle"]
                ),
                "viewing.zenith": viewing_zenith,
                "viewing.azimuth": viewing_azimuth,
                "refzenith": float(dict_of_input.get("L2COMM").get_value("ZenithRef")),
                "refazimuth": float(
                    dict_of_input.get("L2COMM").get_value("AzimuthRef")
                ),
            }

            if l_dircormodel == DirectionalCorrection.ROY:
                LOGGER.debug("Directional Correction Model: ROY ")
                param_dircorr["model"] = "roy"
                param_dircorr["roy.coeffsr"] = xml_tools.as_string_list(
                    dict_of_input.get("L2COMM").get_value("CoefsR")
                )
                param_dircorr["roy.coeffsv"] = xml_tools.as_string_list(
                    dict_of_input.get("L2COMM").get_value("CoefsV")
                )
            elif l_dircormodel == DirectionalCorrection.LUT:
                LOGGER.debug("Directional Correction Model: LUT ")
                param_dircorr["model"] = "lut"
                param_dircorr["lut.filename"] = dict_of_input.get(
                    "DIRCOR"
                ).new_gipp_filename

            dircorr_app = OtbAppHandler("DirectionnalCorrection", param_dircorr)

        cr_lut_file = os.path.join(rlc_working, "CR_Lut.mha")
        param_cr_lut = {
            "lut": dict_of_input.get("L2TOCR"),
            "solar.zenith": float(
                dict_of_input.get("L1Info").SolarAngle["sun_zenith_angle"]
            ),
            "solar.azimuth": float(
                dict_of_input.get("L1Info").SolarAngle["sun_azimuth_angle"]
            ),
            "viewing.zenith": viewing_zenith,
            "viewing.azimuth": viewing_azimuth,
            "reducelut": cr_lut_file,
        }
        if l_dircormodel != DirectionalCorrection.DEACTIVATED:
            param_cr_lut["dircoefs"] = xml_tools.as_string_list(
                dircorr_app.getoutput()["coeffs"]
            )

        OtbAppHandler("ReduceLut", param_cr_lut)
        dict_of_output["cr_lut"] = cr_lut_file
        # Read the input lut header to get indexes
        old_lut = maja_xml_app_lut.parse(dict_of_input.get("L2TOCR"), True)
        # HR lut if wide field
        l_lutmap = LUTMap()
        # Add cr lut to map
        l_listoffile = List_Of_FilesType()
        l_listoffile.add_Relative_File_Path(os.path.basename(cr_lut_file))
        l_lut = LUT(index=0, Indexes=old_lut.get_Indexes(), List_Of_Files=l_listoffile)
        l_lutmap.add_LUT(l_lut)
        if dict_of_input.get("Plugin").WideFieldSensor and l_writeL2:
            m_ViewingZenithAnglesMap = dict_of_input.get("L1Reader").get_value(
                "ViewingZenithMeanMap"
            )
            m_ViewingAzimuthAnglesMap = dict_of_input.get("L1Reader").get_value(
                "ViewingAzimuthMeanMap"
            )
            for key in list(m_ViewingZenithAnglesMap.keys()):
                if key not in m_ViewingAzimuthAnglesMap:
                    raise MajaDataException(
                        "Internal ERROR: The Viewing Zenith angles Map per zone are incorrect !!"
                    )
                l_listoffile = List_Of_FilesType()
                l_listoffile.add_Relative_File_Path("HR_Lut_" + key + ".mha")
                l_lut = LUT(
                    index=key, Indexes=old_lut.get_Indexes(), List_Of_Files=l_listoffile
                )
                l_ViewingZenithAngle = m_ViewingZenithAnglesMap.get(key)
                l_ViewingAzimuthAngle = m_ViewingAzimuthAnglesMap.get(key)
                # LOGGER.debug("Viewing Angles Size : " + str(len(l_ViewingZenithAngle)) + " | Nb bands : " + str(len(dict_of_input.get("Plugin").BandsDefinitions.L2CoarseBandMap)))
                if len(l_ViewingZenithAngle) != len(
                    dict_of_input.get("Plugin").BandsDefinitions.L2CoarseBandMap
                ):
                    # Some viewing angles are missing for one or more bands, in that case the product is malformed but we have to continue processing anyway, we take a mean of all viewing angles for this band
                    L1_dtf_list = dict_of_input.get("L1Info").MuscateData[
                        "ZoneMaskFileNames"
                    ]
                    adjacent_dft_nb = 0
                    if key == sorted(m_ViewingZenithAnglesMap.keys())[0]:
                        adjacent_dft_nb = int(key) + 1
                    elif key == sorted(m_ViewingZenithAnglesMap.keys())[-1]:
                        adjacent_dft_nb = int(key) - 1
                    else:
                        raise MajaDataException(
                            "Error on input L1 product: missing Viewing angles in the center of the image for some l2 coarse band on detector "
                            + key
                        )

                    # if the product doest not contain angles for a particular detector, we can try to find this detector and its adjacent in the product's masks list
                    dtf_wrong, dtf_adjacent = self.find_missing_detector(
                        L1_dtf_list, int(key), adjacent_dft_nb
                    )
                    if dtf_wrong == "" or dtf_adjacent == "":
                        raise MajaDataException(
                            "Error on input L1 product: Missing detector "
                            + key
                            + " file"
                        )

                    # Checking if the adjacent dtf contains the part covered by the dtf with missing angles
                    output_dtf_bandmath = os.path.join(rlc_working, "Bandmath_DTF.tif")
                    output_binary = os.path.join(
                        rlc_working, "DTF_" + key + "_binary.tif"
                    )
                    dtf_bm_app = OtbAppHandler(
                        "BandMath",
                        {
                            "il": [dtf_adjacent, dtf_wrong],
                            "out": output_dtf_bandmath + ":uint8",
                            "exp": "(im1b1>0 && im2b1>0)? 1:0",
                        },
                        write_output=False,
                    )
                    self._lut_pipeline.add_otb_app(dtf_bm_app)
                    binary_dtf_app = OtbAppHandler(
                        "BandMath",
                        {
                            "il": [dtf_wrong],
                            "out": output_binary + ":uint8",
                            "exp": "im1b1 > 0 ? 1:0",
                        },
                        write_output=False,
                    )
                    self._lut_pipeline.add_otb_app(binary_dtf_app)
                    params_compare = {
                        "ref.in": dtf_bm_app.getoutput().get("out"),
                        "meas.in": binary_dtf_app.getoutput().get("out"),
                    }
                    compare_dtf_app = OtbAppHandler(
                        "CompareImages", params_compare, write_output=False
                    )
                    self._lut_pipeline.add_otb_app(compare_dtf_app)
                    if compare_dtf_app.getoutput().get("count") == 0:
                        LOGGER.warning(
                            "Error on input L1 product: missing Viewing angles for some l2 coarse band on detector "
                            + key
                            + " , using angles from adjacent detector to compute HR_Lut generation"
                        )
                        viewZenith = np.array(l_ViewingZenithAngle, dtype=float)
                        viewAzimuth = np.array(l_ViewingAzimuthAngle, dtype=float)
                        meanZenith = np.mean(viewZenith)
                        meanAzimuth = np.mean(viewAzimuth)
                        nb_bands_missing_angles = len(
                            dict_of_input.get("Plugin").BandsDefinitions.L2CoarseBandMap
                        ) - len(l_ViewingZenithAngle)
                        for i in range(0, nb_bands_missing_angles):
                            l_ViewingZenithAngle.append(str(meanZenith))
                            l_ViewingAzimuthAngle.append(str(meanAzimuth))
                    else:
                        raise MajaDataMissingAngleException(
                            "Error on input L1 product: missing Viewing angles for some l2 coarse band on detector "
                            + key
                            + " , all l2 coarse bands are needed for HR_Lut generation"
                        )

                hr_lut_file = os.path.join(rlc_working, "HR_Lut_" + key + ".mha")
                param_hr_lut = {
                    "lut": dict_of_input.get("L2TOCR"),
                    "solar.zenith": float(
                        dict_of_input.get("L1Info").SolarAngle["sun_zenith_angle"]
                    ),
                    "solar.azimuth": float(
                        dict_of_input.get("L1Info").SolarAngle["sun_azimuth_angle"]
                    ),
                    "viewing.zenith": l_ViewingZenithAngle,
                    "viewing.azimuth": l_ViewingAzimuthAngle,
                    "reducelut": hr_lut_file,
                }
                OtbAppHandler("ReduceLut", param_hr_lut)
                dict_of_output["hr_lut_" + key] = hr_lut_file
                l_lutmap.add_LUT(l_lut)
        # Write down the lut map
        output = io.StringIO()
        output.write('<?xml version="1.0" ?>\n')
        l_lutmap.export(output, 0, name_="LUTMap", namespacedef_="", pretty_print=True)
        l_lutmap_filename = os.path.join(rlc_working, "HR_LutMap.xml")
        with open(l_lutmap_filename, "w") as fh:
            fh.write(output.getvalue().replace("    ", "  "))
            LOGGER.info("Writed new gipp lutmap to " + l_lutmap_filename)
        output.close()
        dict_of_output["hr_lutmap"] = l_lutmap_filename
