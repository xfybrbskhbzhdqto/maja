apTvColorScale
~~~~~~~~~~~~~~~~~~~~~~~~

Objectif
********
Validation de l'application "ColorScale"

Description
***********

Le module "ColorScale" permet de convertir une image monobande en une image RGB où les valeurs les plus grandes sont représentées en rouge et les valeurs les petites en bleu.  .


Liste des données d’entrées
***************************

-in tmp_sre_scale_R2.tif?&bands=1
-min -2
-max 40


Liste des produits de sortie
****************************

-out apTvColorScale.tif


Prérequis
*********
Il n’y a pas de prérequis.

Durée attendue
***************
La durée d’exécution de l’essai n’est pas un critère attendu.

Epsilon utilisé sur la non regression
*************************************
0

Vérifications à effectuer
**************************
Le test génère en sortie une image RGB : en rouge les valeurs de pixels les plus élevées, en bleu les valeurs de pixels les plus faibles.

Mise en oeuvre du test
**********************
Ce test est exécuté en lançant la commande :
ctest -R apTvColorScale

Journal d’essai de la recette
*****************************

Notes sur le déroulement du test
--------------------------------
Rien de particulier n’a été noté lors du déroulement du test.

Conclusion du déroulement du test
---------------------------------
RAS

Validation du test
------------------

================== =================
Date de validation    Résultat
04/08/2021             OK
================== =================

Exigences
*********
Ce test couvre les exigences suivantes :
Néant

