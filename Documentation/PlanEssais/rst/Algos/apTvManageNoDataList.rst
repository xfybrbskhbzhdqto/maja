apTvManageNoData
~~~~~~~~~~~~~~~~

Objectif
********
Validation de l'application "ManageNoData"

Description
***********

Le module "ManageNoData" contient plusieurs modes: il permet soit de générer un mask de nodata à partir de la valeur nodata de l'image d'entrée, soit de changer la valeur de nodata de l'image d'entrée, soit d'appliquer un mask de nodata à l'image d'entrée. 



Liste des données d’entrées
***************************

Image lambda, un mask lambda et une valeur de nodata.

Liste des produits de sortie
****************************
Image générée en appliquant le mask et la valeur nodata.


Prérequis
*********
Il n’y a pas de prérequis.

Durée attendue
***************
La durée d’exécution de l’essai n’est pas un critère attendu.

Epsilon utilisé sur la non regression
*************************************
0

Vérifications à effectuer
**************************
Le test génère en sortie des statistiques en accord avec le contenu de l'image.

Mise en oeuvre du test
**********************

Ce test est exécuté en lançant la commande :
ctest -R apTvManageNoData

Journal d’essai de la recette
*****************************

Notes sur le déroulement du test
--------------------------------
Rien de particulier n’a été noté lors du déroulement du test.

Conclusion du déroulement du test
---------------------------------
RAS

Validation du test
------------------

================== =================
Date de validation    Résultat
26/07/2021              OK
================== =================

Exigences
*********
Ce test couvre les exigences suivantes :
Néant
